var btnFull = $("#btn-full");
var btnEasy = $("#btn-easy");
var cursor = $("#cursor");

var markerEasy = $("#marker-easy");
var markerFull = $("#marker-full");

var blockEasy = $("#block-easy");
var blockFull = $("#block-full");

var btnFullOnClass = "button-full";
var btnFullOffClass = "button-full-off";
var btnEasyOnClass = "button-easy";
var btnEasyOffClass = "button-easy-off";

btnEasy.on("click", function (){
	var active = parseInt($(this).attr("active"));
	if (!active) {
		deactivateButton(btnFull, btnFullOnClass, btnFullOffClass);
		markerFull.hide();
		blockFull.hide();

		activateButton($(this), btnEasyOnClass, btnEasyOffClass);
		markerEasy.show();
		blockEasy.show();
	}
});

btnFull.on("click", function (){
	var active = parseInt($(this).attr("active"));
	if (!active) {
		deactivateButton(btnEasy, btnEasyOnClass, btnEasyOffClass);
		markerEasy.hide();
		blockEasy.hide();

		activateButton($(this), btnFullOnClass, btnFullOffClass);
		markerFull.show();
		blockFull.show();
	}
});

$("#btn-see-easy-mode").on("click", function() {
	btnEasy.click();
});

$("#btn-see-full-mode").on("click", function() {
	btnFull.click();
});

function activateButton(btn, onClass, offClass)
{
	btn.removeClass(offClass);
	btn.addClass(onClass);
	btn.attr("active", 1);
}

function deactivateButton(btn, onClass, offClass)
{
	btn.removeClass(onClass);
	btn.addClass(offClass);
	btn.attr("active", 0);
}
function changeButtonsText(text)
{
    $('.button').each(function (key, btn) {
        var iconHtml = $(btn).find('i')[0].outerHTML;
        $(btn).html(iconHtml + text);
    })
}

changeSizeHandler = function () {
    var mq = window.matchMedia('screen and (max-width: 768px)');
    if (mq.matches) {
        changeButtonsText("Buy Now");
    } else {
        changeButtonsText("Download for Free");
    }
}
$(window).resize(changeSizeHandler);
changeSizeHandler();